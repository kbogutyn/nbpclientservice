## Zadanie 
Celem zadania jest przygotowanie mikroserwisu, który udostępni (poprzez API REST) notowania kursów walut dla wszystkich systemów firmy X. 
Jako źródło danych przyjmujemy kursy średnie walut obcych udostępniane przez Narodowy Bank Polski na stronie nbp.pl 
Mikroserwis musi być gotowy na sprawne obsłużenie dużej ilości żądań dziennie - często będzie to wielokrotne pobranie kursu tej samej waluty. 
## Endpointy 
Mikroserwis powinien udostępniać następujące endpointy: 
lista dostępnych walut wraz z kodem waluty, najnowszy, średni kurs NBP dla wybranej waluty (w parametrach przekazany kod waluty), średni kursu dla wybranej waluty, obliczony na podstawie wszystkich pobranych wcześniej (od początku życia usługi) średnich kursów NBP danej waluty (w parametrach przekazany kod waluty). 
## Dodatkowe wytyczne 
zastosuj dowolnie wybrany, znany Ci framework PHP, mikroserwis powinien wykorzystywać warstwę persystencji danych, staraj się wykonać zadanie wedle swojej najlepszej wiedzy, oceniane będzie nie tylko to "czy działa", ale także to w jaki sposób zadanie zostało wykonane. 
