<?php

namespace App\Tests\unit\Service;

use App\Entity\Rate;
use App\Services\Calculator\AverageCalculator;
use PHPUnit\Framework\TestCase;

class AverageCalculatorTest extends TestCase
{
    private $calculator;

    public function setUp()
    {
        $this->calculator = new AverageCalculator();
        parent::setUp();
    }

    public function test_calculate_average_rate()
    {
        $rate1 = new Rate();
        $rate1->setMid(6.2);
        $rate2 = new Rate();
        $rate2->setMid(5.4);
        $rates = [$rate1, $rate2];

        $result = $this->calculator->calculate($rates);
        $this->assertEquals(5.8, $result);
    }
}