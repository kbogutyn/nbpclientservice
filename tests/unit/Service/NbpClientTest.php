<?php

namespace App\Tests\unit\Service;

use App\Services\Client\Http\HttpClient;
use App\Services\Client\Http\Response;
use App\Services\Client\NbpClient;
use PHPUnit\Framework\TestCase;

class NbpClientTest extends TestCase
{
    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var NbpClient
     */
    private $client;

    public function setUp()
    {
        $this->httpClient = $this->createMock(HttpClient::class);
        $this->client = new NbpClient($this->httpClient);
    }

    public function test_get_currency_rate()
    {
        $this->httpClient
            ->expects($this->once())
            ->method('get')
            ->willReturn(new Response(200, ''));

        $this->client->getCurrencyRate('eur');
    }

    public function test_get_currencies()
    {
        $this->httpClient
            ->expects($this->once())
            ->method('get')
            ->willReturn(new Response());

        $this->client->getCurrencies();
    }
}