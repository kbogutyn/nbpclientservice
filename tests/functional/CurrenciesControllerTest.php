<?php

namespace App\Tests\functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;

class CurrenciesControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->client->followRedirects(true);

        parent::setUp();
    }

    public function test_view_list_of_available_endpoints()
    {
        $this->client->request('GET', '/');

        $this->assertStatusCode(200, $this->client);
        $this->assertJson($this->client->getResponse()->getContent());
        $this->assertContains('/currencies', $this->client->getResponse()->getContent());
        $this->assertContains('/currencies/{currencyCode}', $this->client->getResponse()->getContent());
        $this->assertContains('/currencies/average/{currencyCode}', $this->client->getResponse()->getContent());
    }


    public function test_view_single_currency_rate()
    {
        $this->client->request('GET', '/currencies/eur');
        $this->assertStatusCode(200, $this->client);
        $this->assertJson($this->client->getResponse()->getContent());
        $this->assertContains("rate", $this->client->getResponse()->getContent());
    }

    public function test_view_currency_rate_with_invalid_currency_code_returns_http404()
    {
        $this->client->request('GET', '/currencies/asd');
        $this->assertStatusCode(404, $this->client);
        $this->assertJson($this->client->getResponse()->getContent());
        $this->assertContains("errorMessage", $this->client->getResponse()->getContent());
    }

    public function test_view_list_of_currencies_rate()
    {
        $this->client->request('GET', '/currencies');
        $this->assertStatusCode(200, $this->client);
        $this->assertJson($this->client->getResponse()->getContent());
        $this->assertContains('"currency": "euro",', $this->client->getResponse()->getContent());
    }

    public function test_view_average_currency_rate()
    {
        $this->client->request('GET', '/currencies/average/eur');
        $this->assertStatusCode(200, $this->client);
        $this->assertJson($this->client->getResponse()->getContent());
        $this->assertContains('average', $this->client->getResponse()->getContent());
    }

    private function assertStatusCode($code, Client $client)
    {
        $this->assertEquals($code, $client->getResponse()->getStatusCode());
    }
}