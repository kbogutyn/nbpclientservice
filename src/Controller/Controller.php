<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

abstract class Controller extends \Symfony\Bundle\FrameworkBundle\Controller\Controller
{
    public function createJsonResponse(array $data, int $code = 200): JsonResponse
    {
        $response = new JsonResponse($data, $code);
        $response->setEncodingOptions($response->getEncodingOptions() | JSON_PRETTY_PRINT);
        $response->setEncodingOptions($response->getEncodingOptions() | JSON_UNESCAPED_SLASHES);

        return $response;
    }
}
