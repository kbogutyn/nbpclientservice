<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Rate;
use App\Model\Currency;
use App\Model\CurrencyList;
use App\Model\Rates;
use App\Services\Calculator\AverageCalculator;
use App\Services\Client\NbpClient;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

class CurrenciesController extends Controller
{
    public $client;

    public $serializer;

    public $em;

    public $logger;

    public function __construct(
        NbpClient $client,
        Serializer $serializer,
        EntityManagerInterface $em,
        LoggerInterface $logger)
    {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index()
    {
        $availableEndpoint = [
            'Available endpoints' =>
                [
                    '/currencies' => 'Returns all available currencies with codes.',
                    '/currencies/{currencyCode}' => 'The newest single currency rate from National Bank of Poland.',
                    '/currencies/average/{currencyCode}' => 'Average currency rate based on all historical requests.'
                ]
        ];

        return $this->createJsonResponse($availableEndpoint);
    }

    /**
     * @Route("/currencies", name="currencies", methods={"GET"})
     */
    public function currencies(AdapterInterface $cache)
    {
        $response = $this->client->getCurrencies();
        $item = $cache->getItem('currencies');

        if (!$item->isHit()) {

            if ($response->getResponseCode() === 200) {
                $data = $this->extractCurrenciesFromResponse($response->getResponseBody());
                $currency = $this->serializer->deserialize($data, CurrencyList::class, 'json');

                $item->set($currency->getRates());
                $item->expiresAt(new \DateTime('+60 seconds'));
                $cache->save($item);

                return $this->createJsonResponse($currency->getRates());
            }
        }
        return $this->createJsonResponse($item->get());
    }

    /**
     * @Route("/currencies/{currencyCode}", name="currency.rate", methods={"GET"})
     */
    public function currencyRate(string $currencyCode, AdapterInterface $cache): Response
    {
        $response = $this->client->getCurrencyRate($currencyCode);
        $item = $cache->getItem($currencyCode);

        if (!$item->isHit()) {

            if ($response->getResponseCode() === 200) {
                $currency = $this->serializer->deserialize($response->getResponseBody(), Currency::class, 'json');
                $rate = $this->serializer->deserialize($currency->getRates(), Rates::class, 'json');

                $newRate = $this->createRateFromModel($currency, $rate);
                $item->set($newRate->getMid());
                $item->expiresAt(new \DateTime('+60 seconds'));
                $cache->save($item);

                try {
                    $this->em->persist($newRate);
                    $this->em->flush();
                } catch (\Throwable $e) {
                    $this->logger->error('Unable to save currency rate', $e);
                }

                return $this->createJsonResponse(['rate' => $newRate->getMid()]);
            }

            return $this->createJsonResponse(['errorMessage' => $response->getResponseBody()], $response->getResponseCode());
        }

        return $this->createJsonResponse(['rate' => $item->get()]);
    }

    /**
     * @Route("/currencies/average/{currencyCode}", name="currency.average_rate", methods={"GET"})
     */
    public function averageCurrencyRate(AverageCalculator $calculator, AdapterInterface $cache, string $currencyCode)
    {
        $item = $cache->getItem('average_'.$currencyCode);

        if (!$item->isHit()) {
            $rates = $this->em->getRepository(Rate::class)->findBy(['code' => $currencyCode]);
            $average = $calculator->calculate($rates);

            $item->set($average);
            $item->expiresAt(new \DateTime('+1 hour'));
            $cache->save($item);

            return $this->createJsonResponse(['average' => $average]);
        }

        return $this->createJsonResponse(['average' => $item->get()]);
    }

    private function createRateFromModel(Currency $currency, Rates $rates): Rate
    {
        $rate = new Rate();
        $rate->setCode($currency->getCode());
        $rate->setCurrency($currency->getCurrency());
        $rate->setMid($rates->getMid());

        return $rate;
    }

    private function extractCurrenciesFromResponse(string $responseBody)
    {
        $json = json_decode($responseBody);
        return json_encode($json[0]);
    }
}
