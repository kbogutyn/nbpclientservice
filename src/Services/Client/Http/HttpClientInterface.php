<?php declare(strict_types=1);

namespace App\Services\Client\Http;

interface HttpClientInterface
{
    public function get(string $url);
}