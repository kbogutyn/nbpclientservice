<?php declare(strict_types=1);

namespace App\Services\Client\Http;

class HttpClient implements HttpClientInterface
{
    public const TABLES_ENDPOINT = 'http://api.nbp.pl/api/exchangerates/tables/A/';
    public const RATES_ENDPOINT = 'http://api.nbp.pl/api/exchangerates/rates/A/';


    public function get(string $url): Response
    {
        $ch = $this->getCurlHandler();
        curl_setopt($ch, CURLOPT_URL, $url);

        $response = curl_exec($ch);

        if ($response === false) {
            throw new \RuntimeException(
                sprintf('Request Failed. Curl error number: "%s, message: "%s"', curl_errno($ch), curl_error($ch))
            );
        }

        $responseCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        if ($responseCode < 200 || $responseCode >= 500) {
            throw new \RuntimeException(sprintf('Request failed on remote end; Response: "%s".', $response));
        }

        return new Response($responseCode, $response);
    }

    /**
     * @return resource
     */
    private function getCurlHandler()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        return $ch;
    }
}
