<?php declare(strict_types=1);

namespace App\Services\Client\Http;

class Response
{
    private $responseCode;

    private $responseBody;

    public function __construct(int $responseCode, string $responseBody)
    {
        $this->responseCode = $responseCode;
        $this->responseBody = $responseBody;
    }

    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    public function getResponseBody(): string
    {
        return $this->responseBody;
    }

    public function getMessage(): ?string
    {
        if ($this->responseBody === null) {
            return null;
        }

        $decoded = json_decode($this->responseBody, true);

        if ($this->isError() && isset($decoded['message'])) {
            return $decoded['message'];
        }

        return null;
    }
}
