<?php declare(strict_types=1);

namespace App\Services\Client;

use App\Services\Client\Http\HttpClient;
use App\Services\Client\Http\HttpClientInterface;
use App\Services\Client\Http\Response;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class NbpClient implements LoggerAwareInterface
{
    private $httpClient;

    private $logger;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->logger = new NullLogger();
    }

    public function getCurrencies(): Response
    {
        try {
            return $this->httpClient->get(HttpClient::TABLES_ENDPOINT);
        } catch (\RuntimeException $exception) {
            $this->logger->error('Unable to get currencies rates table', $exception);
        }
    }

    public function getCurrencyRate(string $currency): Response
    {
        try {
            return $this->httpClient->get(HttpClient::RATES_ENDPOINT.$currency);
        } catch (\RuntimeException $exception) {
            $this->logger->error('Unable to get currencies rates table', $exception);
        }
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}