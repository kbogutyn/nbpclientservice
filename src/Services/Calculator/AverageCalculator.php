<?php declare(strict_types=1);

namespace App\Services\Calculator;

use App\Entity\Rate;

class AverageCalculator
{
    public function calculate(iterable $rates)
    {
        $sum = 0;
        $count = 0;
        foreach ($rates as $rate) {
            if ($rate instanceof Rate) {
                $sum += $rate->getMid();
                $count++;
            }
        }

        if ($count > 0) {
            return round($sum/$count, 4);
        }

        return 0;
    }
}