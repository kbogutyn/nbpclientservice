<?php declare(strict_types=1);

namespace App\Model;

class CurrencyList
{
    private $rates;

    public function getRates(): array
    {
        return $this->rates;
    }

    public function setRates(array $rates): void
    {
        $this->rates = $rates;
    }
}
