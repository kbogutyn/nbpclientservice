<?php declare(strict_types=1);

namespace App\Model;

class Currency
{
    private $table;

    private $currency;

    private $code;

    private $rates;

    public function getTable(): string
    {
        return $this->table;
    }

    public function setTable(string $table): void
    {
        $this->table = $table;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getRates()
    {
        return $this->rates;
    }

    public function setRates($rates): void
    {
        $this->rates = json_encode($rates[0]);
    }
}