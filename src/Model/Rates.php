<?php declare(strict_types=1);

namespace App\Model;

class Rates
{
    private $effectiveDate;

    private $mid;

    public function getEffectiveDate(): string
    {
        return $this->effectiveDate;
    }

    public function setEffectiveDate(string $effectiveDate): void
    {
        $this->effectiveDate = $effectiveDate;
    }

    public function getMid(): string
    {
        return $this->mid;
    }

    public function setMid(string $mid): void
    {
        $this->mid = $mid;
    }
}
